import json
import os

from faker import Faker


class JsonUtil:

    fake = Faker()

    def get_json(self, json_name):
        """
         :type schema_name: file schema
         @param schema_name:
         @return:
        """
        absolute_path = os.path.dirname(os.path.realpath(__file__)) + os.sep + "schemas" + os.sep + json_name
        with open(absolute_path) as read_file:
            json_string = json.dumps(json.loads(read_file.read()))
            return json_string

    def update_user_json(self, json_name):
        """
        @param json_name:
        @return: json
        """
        with open(os.path.dirname(os.path.realpath(__file__)) + os.sep + 'schemas' + os.sep + json_name,
                  'r+') as f:
            data = json.load(f)
            data['email'] = self.fake.email()  # <--- add `id` value.
            f.seek(0)  # <--- should reset file position to the beginning.
            json.dump(data, f, indent=4)
            f.truncate()  # remove remaining part

        file = open(os.path.dirname(os.path.realpath(__file__)) + os.sep + 'schemas' + os.sep + json_name, 'r')
        json_imput = file.read()
        request_json_body = json.loads(json_imput)
        json_string = json.dumps(request_json_body)

        return json_string
