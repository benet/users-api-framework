import json
import os
import pytest
import requests
from faker import Faker
from jsonschema import validate, ValidationError, SchemaError
from utils.json_util import JsonUtil

myjson = JsonUtil()
fake = Faker()
URL = "https://serverest.dev/usuarios"


@pytest.fixture()
def search_user():
    """
    get user for tests
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
    }

    response = requests.get(URL, headers=headers)
    resp_dict = response.json()
    user_id = resp_dict['usuarios'][0]['_id']
    return user_id


@pytest.mark.order(1)
@pytest.mark.functional
def test_get_user():
    """
     Validate get users
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
    }

    response = requests.get(URL, headers=headers)
    resp_dict = response.json()

    assert resp_dict['quantidade'] != 0
    assert response.status_code == 200


@pytest.mark.order(2)
@pytest.mark.functional
def test_get_specific_user(search_user):
    """
     Validate get specific user
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
    }
    id_user = search_user
    response = requests.get(URL + '/' + id_user, headers=headers)

    assert response.status_code == 200


@pytest.mark.order(3)
@pytest.mark.functional
def test_put_user(search_user):
    """
     Validate update user
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
    }
    id_user = search_user
    response = requests.put(URL + '/' + id_user, data=myjson.update_user_json('put_user.json'), headers=headers)
    resp_dict = response.json()

    assert resp_dict['message'] == 'Registro alterado com sucesso'
    assert response.status_code == 200


@pytest.mark.order(4)
@pytest.mark.functional
def test_post_user():
    """
     Validate add new user
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
    }
    id_user = search_user
    response = requests.post(URL, data=myjson.update_user_json('post_user.json'), headers=headers)
    resp_dict = response.json()

    assert resp_dict['message'] == 'Cadastro realizado com sucesso'
    assert response.status_code == 201


@pytest.mark.order(5)
@pytest.mark.functional
def test_delete_user(search_user):
    """
     Validate delete user
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
    }
    id_user = search_user
    response = requests.delete(URL + '/' + id_user, headers=headers)
    resp_dict = response.json()

    assert resp_dict['message'] == 'Registro excluído com sucesso'
    assert response.status_code == 200  # -> atualizar status code para 204, convencao


@pytest.mark.contract
def test_contract_user():
    """
    Valid contract POST user
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8',
    }
    id_user = search_user
    response = requests.post(URL, data=myjson.update_user_json('post_user.json'), headers=headers)
    response_json = json.loads(response.text)

    schema = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'utils' + os.sep + 'schemas' + os.sep + 'post_contract_user.json'
    schema_file = open(schema, 'r')
    json_schema = json.loads(schema_file.read())

    try:
        validate(response_json, json_schema)

    except SchemaError as e:
        pytest.fail("There is an error with the path schema: {}".format(e))
        print(e.absolute_path)
        print(e.absolute_schema_path)

    except ValidationError as e:
        pytest.fail("There is an error with the schema: {}".format(e))


@pytest.mark.health
def test_health_api():
    """
     Validate health api
    """
    headers = {
        'Content-Type': 'application/json; charset=utf-8'
    }
    response = requests.get(URL, headers=headers)

    assert response.status_code == 200


if __name__ == '__main__':
    pass